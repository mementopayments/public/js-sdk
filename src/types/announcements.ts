export type AnnouncementType = "informational" | "mandatory" | "blocking";

export interface Announcement {
  id: string;
  title: string;
  message: string;
  type: AnnouncementType;
  button_url: string;
  button_label: string;
  is_fullscreen: boolean;
  webview_url: string;
}
