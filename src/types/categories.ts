import { Filter, SearchFilter } from "./filters";

export interface Category {
  id: string;
  project_id: string;
  title: string;
  code: string;
  enabled: boolean;
  created_at: string | Date;
  updated_at: string | Date;
  deleted_at: string | Date;
  category_subgroup_ids?: string[];
  category_group?: CategoryGroup;
}

export interface CategoryGroup {
  id: string;
  code: string;
  created_at: string | Date;
  deleted_at: string | Date;
  title: string;
}

export type CategoryFilter = Filter<{
  search?: SearchFilter;
}>;
