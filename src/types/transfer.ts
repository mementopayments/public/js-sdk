import { FundingSourceType } from "./funding_source";
import { Meta } from "./meta";

export type TransferStatus =
  | "created"
  | "awaiting-settlement"
  | "settled"
  | "cancelled"
  | "rejected"
  | "failed"
  | "expired";

export interface TransferType {
  title: string;
  shortcode: string;
  directions: TransferDirection[];
  funding_source_type: FundingSourceType;
  counter_funding_source_type: FundingSourceType;
}

export interface Transfer {
  id: string;
  project_id: string;
  intergration_id: string;
  type: string;
  user_id: string;
  direction_id: string;
  status_id: string;
  status_comments?: string;
  funding_source_id: string;
  counter_funding_source_id: string;
  external_id?: string;
  amount: number;
  currency: string;
  description: string;
  meta: Meta;
  expires_at: Date | string;
}

export interface TransferInput {
  type?: string;
  direction?: string;
  funding_source_id?: string;
  counter_funding_source_id?: string;
  amount?: string;
  currency?: string;
  description?: string;
  meta?: Meta;
}

export interface TransferDirection {
  id: string;
  title: string;
}
