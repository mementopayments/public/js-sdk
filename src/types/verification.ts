import * as core from "./integrations/core";
export * as core from "./integrations/core";

export type VerificationStatus =
  | "created"
  | "processing"
  | "approved"
  | "cancelled"
  | "rejected"
  | "failed"
  | "pending";

export type VerificationDefintions = core.Verifications;

export type VerificationType = VerificationDefintions["type"];

// Export a composed type for improved type hinting
export type VerificationInput = VerificationDefintions["initial_data"];

// Export a composed type for improved type hinting
export type VerificationProvideData = VerificationDefintions["provide_data"];

// Export a composed type for improved type hinting
export type Verification = VerificationDefintions["response"];
