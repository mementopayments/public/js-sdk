import { VerificationType, VerificationStatus } from "..";

export interface BaseVerification<
  T extends VerificationType,
  TPublicData = undefined,
> {
  id: string;
  created_at: string;
  updated_at: string;
  type: T;
  status: VerificationStatus;
  public_data: TPublicData;
}

export interface VerificationInitialData<T extends VerificationType, TData> {
  type: T;
  onboarding_id?: string;
  data: TData;
}

export interface VerificationDefinition<
  Type extends VerificationType,
  Response extends BaseVerification<Type, PublicData>,
  InitialData extends VerificationInitialData<Type, undefined | {}>,
  ProvideData,
  PublicData = undefined,
> {
  type: Type;
  response: Response;
  initial_data: InitialData;
  provide_data: ProvideData;
  public_data: PublicData;
}
