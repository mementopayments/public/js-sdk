import {
  BaseVerification,
  VerificationDefinition,
  VerificationInitialData,
} from "./base";

/*
  Shared code verification definitions
*/

interface CodeVerification {
  code: string;
}

/*
  Phone verification definition
*/

export type PhoneVerificationType = "phone";

export type PhoneVerificationResponse = BaseVerification<PhoneVerificationType>;

export interface PhoneVerificationInitialData
  extends VerificationInitialData<
    PhoneVerificationType,
    {
      phone_number: string;
    }
  > {}

export type PhoneVerificationProvideData = CodeVerification;

export type PhoneVerification = VerificationDefinition<
  PhoneVerificationType,
  PhoneVerificationResponse,
  PhoneVerificationInitialData,
  PhoneVerificationProvideData
>;

/*
  Email verification definition
*/

export type EmailVerificationType = "email";

export type EmailVerificationResponse = BaseVerification<EmailVerificationType>;

export interface EmailVerificationInitialData
  extends VerificationInitialData<
    EmailVerificationType,
    {
      email: string;
    }
  > {}

export type EmailVerificationProvideData = CodeVerification;

export type EmailVerification = VerificationDefinition<
  EmailVerificationType,
  EmailVerificationResponse,
  EmailVerificationInitialData,
  EmailVerificationProvideData
>;

/*
  ResetSecret verification definition
*/

export type ResetSecretVerificationType = "reset_secret";

export type ResetSecretVerificationResponse =
  BaseVerification<ResetSecretVerificationType>;

export interface ResetSecretVerificationInitialData
  extends VerificationInitialData<
    ResetSecretVerificationType,
    {
      email: string;
      type: "password" | "pin";
    }
  > {}

// ResetSecretProvideData is follows the CodeVerification process with the added
// fields new_pin and new_password. You need at least one of new_pin or
// new_password.
export type ResetSecretVerificationProvideData = CodeVerification &
  ({ new_pin: string } | { new_password: string });

export type ResetSecretVerification = VerificationDefinition<
  ResetSecretVerificationType,
  ResetSecretVerificationResponse,
  ResetSecretVerificationInitialData,
  ResetSecretVerificationProvideData
>;

export type PinVerificationType = "pin";

export type PinVerificationResponse = BaseVerification<PinVerificationType>;

export interface PinVerificationInitialData
  extends VerificationInitialData<
    PinVerificationType,
    {
      pin: string;
    }
  > {}

export type PinVerification = VerificationDefinition<
  PinVerificationType,
  PinVerificationResponse,
  PinVerificationInitialData,
  never
>;

export type SignatureVerificationType = "signature";

export type SignatureVerificationResponse =
  BaseVerification<SignatureVerificationType>;

export interface SignatureVerificationInitialData
  extends VerificationInitialData<
    SignatureVerificationType,
    {
      payload: string;
      signature: string;
    }
  > {}

export type SignatureVerification = VerificationDefinition<
  SignatureVerificationType,
  SignatureVerificationResponse,
  SignatureVerificationInitialData,
  never
>;

/*
  Combined exports
*/

export type Verifications =
  | PhoneVerification
  | EmailVerification
  | ResetSecretVerification
  | PinVerification
  | SignatureVerification;

export type SingleStepVerifications = PinVerification | SignatureVerification;
export type SingleStepVerificationInput =
  SingleStepVerifications["initial_data"];
