import { AxiosRequestConfig, AxiosResponse } from "axios";
import { parseLinkHeader } from "../utils/headers";

type MPPLink<T> = Promise<MPPResult<T>>;
type doRequestFunc<T> = (config: AxiosRequestConfig) => Promise<MPPResult<T>>;

interface Links<T> {
  // Add type hints for the most common link occurrences
  next?: () => MPPLink<T>;
  prev?: () => MPPLink<T>;
  first?: () => MPPLink<T>;
  last?: () => MPPLink<T>;

  [key: string]: (() => MPPLink<T>) | undefined;
}

export class MPPResult<T> {
  data: T;
  readonly request_id: string;
  readonly fetched_at: Date;

  readonly links: Links<T>;

  constructor(response: AxiosResponse<T>, doRequest?: doRequestFunc<T>) {
    this.data = response.data;
    this.request_id = response.headers["request-id"];
    this.fetched_at = new Date();

    this.links = {};

    if (doRequest) {
      const linkHeader: string | undefined = response.headers.link;
      if (linkHeader) {
        const links = parseLinkHeader(linkHeader);
        for (const link of links) {
          const rel = link.attrs["rel"];
          if (rel) {
            this.links[rel] = () =>
              doRequest({
                url: link.uri,
                method: "GET",
              });
          }
        }
      }
    }
  }
}
