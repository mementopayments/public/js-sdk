export interface ProjectInvite {
  role: string;
  project_title: string;
}
