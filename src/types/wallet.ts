export interface AppleWalletExtensionData {
  projectId: string;
  email: string;
  authToken: AppleWalletExtensionAuthToken;
  cards: AppleWalletExtensionCard[];
}

export interface AppleWalletExtensionAuthToken {
  token: string;
  expiresAt: Date | string;
}

export interface AppleWalletExtensionCard {
  identifier: string;
  last4: string;
  title: string;
  cardHolderName: string;
  art: string;
}
