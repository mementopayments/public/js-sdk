import { Coordinates, FundingSource, Image, Team } from ".";
import { Category } from "./categories";
import { DateFilter, Filter, StringFilter } from "./filters";

export interface TransactionLocation {
  city: string;
  country: string;
  coordinates?: Coordinates;
}

export interface TransactionMerchant {
  mcc: string;
  mcc_description: string;
  name: string;
  location?: TransactionLocation;
}

export interface TransactionAttributeInfo {
  key: string;
  field: string;
  value: string;
}

export interface TransactionAttributes {
  info: TransactionAttributeInfo[];
}

export type TransactionFilter = Filter<{
  transaction_date: DateFilter;
}>;

export interface Transaction {
  id: string;
  description: string;
  billing_amount: string;
  billing_currency: string;
  transaction_amount: string;
  transaction_currency: string;
  merchant?: TransactionMerchant;
  attributes: TransactionAttributes;
  internal?: InternalTransaction;
  created_at: string | Date;
  updated_at: string | Date;
}

export interface InternalTransaction {
  id: string;
  category?: Category;
  description: string;
  funding_source_id: string;
  card?: FundingSource;
  user_id: string;
  external_id: string;
  transaction_amount: string;
  transaction_currency: string;
  transaction_date: string | Date;
  original_amount: string;
  original_currency: string;
  created_at: string | Date;
  updated_at: string | Date;
  booked_date: string | Date;
  comments: string;
  images: Image[];
  status?: InternalTransactionStatus;
  receipt_id?: string;
  rule_violations: RuleViolation[];
  receipts: TransactionReceipt[];
  sub_categories: {
    category_group_id: string;
    category: Category;
  }[];
  team?: Team;
  team_category?: Category;
}

export type InternalTransactionStatus = "missing_data" | "awaiting_approval";

export interface TransactionInput {
  comments?: string;
  category_id?: string;
  subcategory_ids?: string[];
}

export type InternalTransactionFilter = Filter<{
  funding_source_id?: StringFilter;
}>;

export interface RuleViolation {
  rule_id: number;
  message: string;
}

export interface TransactionReceipt {
  id: string;
  transaction_id: string;
  user_id: string;
  url: string;
  file_name: string;
  file_type: string;
  file_size: number;
  created_at: string | Date;
  updated_at: string | Date;
}

export interface ReceiptUploadMethods {
  file: Blob;
}
