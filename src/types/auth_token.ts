import { DeviceInput } from "./device";

/**
 * Fields that are on all token inputs
 */
interface BaseAuthTokenInput {
  identity?: {
    type: "phone" | "username" | "email";
    value: string;
  };
  device: DeviceInput;
}

/**
 * Input for authenticating with password
 */
interface PasswordAuthTokenInput extends BaseAuthTokenInput {
  authenticator: "password";
  secret: string;
}

/**
 * Input for authenticating with sms
 */
interface SmsAuthTokenInput extends BaseAuthTokenInput {
  authenticator: "sms";
}

/**
 * Combined auth token input type
 */
export type AuthTokenInput = PasswordAuthTokenInput | SmsAuthTokenInput;

/**
 * Secret input used for verifying auth tokens
 */
export interface SecretInput {
  secret?: string;
  pin: string;
}

/**
 * AuthToken returned from the server
 */
export interface AuthToken {
  id: string;
  device_id: string;
  status: string;
  token: string;
  expires_at: Date | string;
  /**
   * The underlying verification id if there is any. Can be used to complete the
   * verification before entering the PIN.
   */
  verification_id?: string;
}
