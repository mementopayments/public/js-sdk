import { Category } from "./categories";
import { Filter, StringFilter } from "./filters";

export type TeamFilter = Filter<
  {
    name?: StringFilter;
  },
  "name" | "created_at"
>;

export interface Team {
  id: string;
  name: string;
  category: Category;
  created_at: string | Date;
  updated_at: string | Date;
}
