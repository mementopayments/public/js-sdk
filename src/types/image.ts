export interface Image {
  id: string;
  url: string;
  full_screen_url: string;
  thumbnail_url: string;
  created_at: string;
  updated_at: string;
}

type ImageInputId = { id: string };
type ImageInputUrl = { url: string };

export type ImageInput = ImageInputId | ImageInputUrl;

export interface ImageOptions {
  file?: Blob;
  url?: string;
  encoded?: string;
  user_image?: boolean;
}

interface ImageUploadFile {
  file: Blob;
}

interface ImageUploadUrl {
  url: string;
}

interface ImageUploadEncoded {
  encoded: string;
}

export type ImageUploadMethods =
  | ImageUploadFile
  | ImageUploadUrl
  | ImageUploadEncoded;
