import { Owner } from "./owner";

export interface Referral {
  id: string;
  code: string;
  campaign_id: string;
  expires_at: string | Date;
  created_at: string | Date;
  updated_at: string | Date;
  owner: Owner;
}

export interface ReferralInput {
  campaign_id: string;
  phone?: string;
  email?: string;
}
