import { AuthToken, DeviceInput, Verification, VerificationType } from ".";
import { AddressInput } from "./address";

export const OnboardingAttributeKind = {
  FullName: "@mpp/FULL_NAME",
  PreferredName: "@mpp/PREFERRED_NAME",
  DateOfBirth: "@mpp/DATE_OF_BIRTH",
  Email: "@mpp/EMAIL",
  Username: "@mpp/USERNAME",
  NIN: "@mpp/NIN",
  Phone: "@mpp/PHONE",
  AppPIN: "@mpp/APP_PIN",
  Password: "@mpp/PASSWORD",
  Address: "@mpp/ADDRESS",
  Device: "@mpp/DEVICE",
} as const;

type StringAttributes =
  | typeof OnboardingAttributeKind.FullName
  | typeof OnboardingAttributeKind.PreferredName
  | typeof OnboardingAttributeKind.DateOfBirth
  | typeof OnboardingAttributeKind.Email
  | typeof OnboardingAttributeKind.Username
  | typeof OnboardingAttributeKind.NIN
  | typeof OnboardingAttributeKind.Phone
  | typeof OnboardingAttributeKind.AppPIN
  | typeof OnboardingAttributeKind.Password;

interface StringAttributeInput {
  kind: StringAttributes;
  value: string;
}

interface AddressAttributeInput {
  kind: typeof OnboardingAttributeKind.Address;
  value: AddressInput;
}

interface DeviceAttributeInput {
  kind: typeof OnboardingAttributeKind.Device;
  value: DeviceInput;
}

export type OnboardingAttributesInput =
  | StringAttributeInput
  | AddressAttributeInput
  | DeviceAttributeInput;

export interface OnboardingAddAttributesInput {
  batch?: string;
  attributes: OnboardingAttributesInput[];
}

export type OnboardingStatus =
  | "pending"
  | "processing"
  | "rejected"
  | "approved"
  | "user_created"
  | "expired";

export interface OnboardingState {
  is_completed: boolean;
  pending_verifications: Verification[];
  missing_verifications: { type: VerificationType; verification_id: string }[];
  required_attributes: OnboardingAttributesInput[];
  missing_attributes: OnboardingAttributesInput[];
}

interface BaseOnboardingResult {
  id: string;
  status: OnboardingStatus;
  onboarding_state: OnboardingState;
  expires_at: string | Date;
}

interface ApprovedOnboardingResult extends BaseOnboardingResult {
  status: "approved";
  user_id: string;
  authentication_token: AuthToken;
}

interface UserCreatedOnboardingResult extends BaseOnboardingResult {
  status: "user_created";
  user_id: string;
  authentication_token: AuthToken;
}

interface NoUserCreatedOnboardingResult extends BaseOnboardingResult {
  status: Exclude<OnboardingStatus, "approved" | "user_created">;
}

export type OnboardingResult =
  | ApprovedOnboardingResult
  | UserCreatedOnboardingResult
  | NoUserCreatedOnboardingResult;

interface OnboardingInviteInput {
  id: string;
  token: string;
}

export interface OnboardingCreateInput {
  device: DeviceInput;
  // Will be required in the future
  email?: string;
  invite?: OnboardingInviteInput;
}
