export interface PublicTokenInput {
  funding_source_type: string;
}

export interface GetPublicTokenResponse {
  token: string;
  expires_at: Date | string;
}
