type EqFilter<T> = { eq: T | T[] };
type NeqFilter<T> = { neq: T | T[] };
type LtFilter<T> = { lt: T };
type LteFilter<T> = { lte: T };
type GtFilter<T> = { gt: T };
type GteFilter<T> = { gte: T };

export type FilterDef<F, T> = { (key: F): T };

export type SearchFilter = EqFilter<string>;
export type StringFilter = EqFilter<string> | NeqFilter<string>;
export type BoolFilter = EqFilter<boolean> | NeqFilter<boolean>;
export type DateFilter = LtFilter<string | Date> | GtFilter<string | Date>;
export type ConstantFilter<T> = EqFilter<T> | NeqFilter<T>;
export type NumberFilter =
  | EqFilter<number>
  | NeqFilter<number>
  | LtFilter<number>
  | LteFilter<number>
  | GtFilter<number>
  | GteFilter<number>;

export type SortDirection = "desc" | "asc";
export type Sort<S extends string> = [S, SortDirection];

export type FilterOp = {
  [key: string]: { [op: string]: any | any[] } | undefined;
};

export interface Filter<
  F extends FilterOp = FilterOp,
  S extends string = "created_at",
> {
  page?: string | number;
  limit?: string | number;
  sort?: Sort<S>[];
  filter?: F | F[];
}
