import { StringFilter, ConstantFilter, Filter } from "./filters";
import { Image } from "./image";
import { Meta } from "./meta";
import { core } from "./verification";

export type FundingSourceStatus =
  | "created"
  | "active"
  | "expired"
  | "rejected"
  | "frozen"
  | "locked"
  | "closed";

export type FundingSourceOperation =
  | "funding-source.activate"
  | "funding-source.can-receive-funds"
  | "funding-source.create"
  | "funding-source.delete"
  | "funding-source.freeze"
  | "funding-source.get-balance"
  | "funding-source.get-transactions"
  | "funding-source.get-authorizations"
  | "funding-source.lock"
  | "funding-source.move_funds"
  | "funding-source.renew"
  | "funding-source.replace"
  | "funding-source.request-pin"
  | "funding-source.set-pin"
  | "funding-source.unfreeze"
  | "funding-source.unlock"
  | "funding-source.update"
  | "funding-source.get-sensitive-details"
  | "funding-source.push-provisioning-apple-pay"
  | "funding-source.push-provisioning-google-pay"
  | "funding-source.in-app-activation";

export type FundingSourceFilter = Filter<{
  status?: ConstantFilter<FundingSourceStatus>;
  // Filter by funding source type shortcode
  type?: StringFilter;
}>;

export interface FundingSource {
  id: string;
  created_at: string;
  updated_at: string;
  parent_id?: string;
  limit_tier: number;
  description: string;
  meta: Meta;
  verified: boolean;
  verified_at: string;
  type: FundingSourceType;
  status: FundingSourceStatus;

  bank_account?: BankAccount;
  card?: Card;
  available_operations: FundingSourceOperation[];
  account_holder_id?: string;
  account_holder?: AccountHolder;
}

export interface FundingSourceBankAccountInput {
  country?: string;
  swift?: string;
  iban?: string;
  account_number?: string;
  owner_nin?: string;
  name?: string;
  routing_number?: string;
  account_type?: string;
  institution_number?: string;
}

export interface FundingSourceCardInput {
  card_type_id?: number;
  owner_nin?: string;
  brand?: string;
  expiration_month?: number;
  expiration_year?: number;
  token?: string;
  masked_number?: string;
  meta?: Meta;
}

export interface FundingSourceInput {
  type?: string;
  status?: FundingSourceStatus;
  description?: string;
  external_id?: string;
  meta?: Meta;
  account_holder_id?: string;
  account_holder?: AccountHolderInput;
  bank_account?: FundingSourceBankAccountInput;
  card?: FundingSourceCardInput;
}

export interface Balance {
  currency: string;
  available_balance: string;
  settled_balance: string;
  period: string;
  limit: string;
  usage_type: string;
  used: string;
}

export interface FundingSourceType {
  id: string;
  created_at: string;
  updated_at: string;
  title: string;
  shortcode: string;
  image: Image;
}

export interface BankAccount {
  id: string;
  created_at: string;
  updated_at: string;
  country?: string;
  swift?: string;
  iban?: string;
  account_number?: string;
  owner_nin?: string;
  name: string;
  institution_number?: string;
  routing_number: string;
  account_type: string;
}

export interface Card {
  id: string;
  created_at: string;
  updated_at: string;
  user_id: string;
  type_id: number;
  brand: string;
  masked_number: string;
  expiration_month: number;
  expiration_year: number;
  token: string;
  meta: Meta;
}

export interface AccountHolder {
  id: string;
  project_id: string;
  user_id: string;
  type_id: string;
  type: AccountHolderType;
  external_id?: string;
  name: string;
  email?: string;
  phone?: string;
}

export interface AccountHolderInput {
  external_id?: string;
  project_id?: string;
  user_id?: string;
  type_id?: string;
  name?: string;
  email?: string;
  phone?: string;
  type?: string;
}

export interface AccountHolderType {
  id: string;
  title: string;
}

export interface FundingSourceOperationInput {
  data?: string;
  message?: string;
  reason?: string;
  verification?: core.SingleStepVerificationInput;
  new_pin?: string;
}

export interface FundingSourceOperationMessage {
  code: string;
  message: string;
}

export interface FundingSourceRequestPinResponse {
  pin: string;
}

export interface FundingSourceSensitiveDetails {
  iframe?: FundingSourceSensitiveDetailsIFrame;
  image?: FundingSourceSensitiveDetailsImage;
  card_details?: CardSensitiveDetails;
}

export interface FundingSourceSensitiveDetailsIFrame {
  url: string;
  token?: string;
  referer?: string;
}

export interface FundingSourceSensitiveDetailsImage {
  url: string;
}

export interface CardSensitiveDetails {
  card_number: string;
  cvv: string;
  expiration_date: string;
}

export interface FundingSourceProvisionApplePayInput {
  certificates: string[];
  nonce: string;
  nonce_signature: string;
}

export interface FundingSourceProvisionApplePay {
  activation_data: string;
  encrypted_pass_data: string;
  ephemeral_public_key: string;
}

export interface FundingSourceProvisionGooglePayInput {
  client_app_id: string;
  client_device_id: string;
  client_wallet_account_id: string;
}

export interface FundingSourceProvisionGooglePay {
  digitization_data: string;
}

export interface FundingSourceInAppActivation {
  activation_data: string;
}
