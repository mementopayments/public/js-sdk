import { Image } from "./image";

export interface Notification {
  id: string;
  created_at: string;
  updated_at: string;
  user_id: string;
  actor_id: string;
  actor: string;
  notification_type: string;
  notification_key: string;
  object_id: string;
  object_data_string: string;
  object_data_number: number;
  description: string;
  image: Image;
  moment_id: string;
}
