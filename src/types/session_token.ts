export interface SessionToken {
  id: string;
  token: string;
  expires_at: Date | string;
  created_at: Date | string;
  updated_at: Date | string;
}
