import { Organization } from "./organization";

export interface Project {
  id: string;
  created_at: string | Date;
  updated_at: string | Date;
  title: string;
  organization?: Organization;
}
