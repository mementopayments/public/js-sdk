import { Image } from "./image";

export interface Owner {
  id: string;
  first_name: string;
  last_name: string;
  full_name: string;
  username: string;
  country: string;
  timezone: string;
  timezone_utc_offset: number;
  verified: boolean;
  official: boolean;
  image?: Image;
}
