export interface Device {
  id: string;
  created_at: string | Date;
  updated_at: string | Date;
  make: string;
  model: string;
  os_name: string;
  os_version: string;
  sdk_version: string;
  screen_width: number;
  screen_height: number;
  apn_device_token: string;
  gcm_device_token: string;
}

export interface DeviceInput {
  make?: string;
  model?: string;
  os_name?: string;
  os_version?: string;
  sdk_version?: string;
  screen_width?: number;
  screen_height?: number;
  apn_device_token?: string;
  gcm_device_token?: string;
}

export interface DeviceTokenInput {
  type: "ios" | "android";
  token: string;
}

export interface DeviceKeyInput {
  /**
   * Users current pin to verify that he can set the device key.
   */
  pin: string;
  /**
   * Base64 encoded RSA 2048 public key in ASN.1 DER format.
   */
  key_data: string;
  /**
   * Sets the expires_at on the key.
   */
  expires_at?: Date;
}

export interface DeviceKey {
  device_id: string;
  /**
   * Base64 encoded RSA 2048 public key in ASN.1 DER format.
   */
  key_data: string;
  expires_at?: string | Date;
  created_at: string | Date;
  deleted_at: string | Date;
}
