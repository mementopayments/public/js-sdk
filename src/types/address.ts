export interface Address {
  id: string;
  created_at: string | Date;
  updated_at: string | Date;
  user_id: string;
  contact_id?: string;
  title: string;
  street_address: string;
  sub_street_address: string;
  locality: string;
  sub_locality: string;
  administrative_area: string;
  sub_administrative_area: string;
  postal_code: string;
  country: string;
  primary: boolean;
}

export interface AddressInput {
  contact_id?: string;
  title?: string;
  street_address?: string;
  sub_street_address?: string;
  locality?: string;
  sub_locality?: string;
  administrative_area?: string;
  sub_administrative_area?: string;
  postal_code?: string;
  country?: string;
  primary?: boolean;
}
