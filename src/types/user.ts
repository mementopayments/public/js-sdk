import { Image, ImageInput } from "./image";
import { DeviceInput } from "./device";
import { AddressInput } from "./address";
import { Meta } from "./meta";
import { OnboardingState } from "./onboarding";

export interface UserSearchInput {
  any?: string[];
  name?: string[];
  email?: string[];
  phone?: string[];
  username?: string[];
  show_current_friends?: boolean;
}

export interface PublicUser {
  id: string;
  created_at: string;
  updated_at: string;
  parent_id?: string;
  first_name: string;
  last_name: string;
  full_name: string;
  username: string;
  status: string;
  verified: boolean;
  official: boolean;
  image?: Image;
}

export interface User {
  id: string;
  created_at: string;
  updated_at: string;
  first_name: string;
  last_name: string;
  full_name: string;
  username: string;
  status: string;
  verified: boolean;
  official: boolean;
  image?: Image;
  type: string;
  limit_tier: number;
  country: string;
  email: string;
  phone: string;
  locale: string;
  preferences: string;
  meta: Meta;
  onboarding_state: OnboardingState;
}

export interface UserInput {
  first_name?: string;
  last_name?: string;
  full_name?: string;
  email?: string;
  username?: string;
  password?: string;
  date_of_birth?: string;
  nin?: string;
  country?: string;
  timezone?: string;
  timezone_utc_offset?: string;
  phone?: string;
  pin?: string;
  new_pin?: string;
  preferences?: string;
  meta?: Meta;
  device?: DeviceInput;
  image?: ImageInput;
  address?: AddressInput;
  verifications?: {
    field: string;
    id: string;
  }[];
  volatile_data?: {
    [key: string]: any;
  };
}
