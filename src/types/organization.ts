import { Address } from "./address";

export interface Organization {
  id: string;
  created_at: string | Date;
  updated_at: string | Date;
  title: string;
  phone: string;
  email: string;
  nin: string;
  address?: Address;
}
