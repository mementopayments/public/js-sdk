import axios, { AxiosError } from "axios";

interface ErrorResponse {
  errors: {
    field?: string;
    code: string;
    message: string;
  }[];
}

export class MPPError extends Error {
  public request_id?: string;
  public data?: ErrorResponse;

  constructor(message: string, aerr?: AxiosError) {
    super(message);
    this.message = this.message;
    this.request_id = aerr?.response?.headers["request-id"];
    this.data = aerr?.response?.data;
  }
}

export class AuthenticationError extends MPPError {
  name = "AuthenticationError";
}
export class GeneralError extends MPPError {
  name = "GeneralError";
}
export class InternalServerError extends MPPError {
  name = "InternalServerError";
}
export class NotFoundError extends MPPError {
  name = "NotFoundError";
}
export class ValidationError extends MPPError {
  name = "ValidationError";
}
export class UnknownError extends MPPError {
  name = "UnknownError";
}

export class LockedUserError extends MPPError {
  name = "LockedUser";
}

export const errorHandler = (err: unknown): MPPError | Error => {
  if (err instanceof Error) {
    if (axios.isAxiosError(err)) {
      let aerr = err as AxiosError;
      if (aerr.response) {
        switch (aerr.response.status) {
          case 400:
            return new GeneralError(aerr.message, aerr);
          case 401:
            return new AuthenticationError(aerr.message, aerr);
          case 404:
            return new NotFoundError(aerr.message, aerr);
          case 422:
            return new ValidationError(aerr.message, aerr);
          case 500:
            return new InternalServerError(aerr.message, aerr);
          default:
            return new UnknownError(aerr.message, aerr);
        }
      } else {
        return aerr;
      }
    }

    return err;
  }

  // Without a response this is unknown, presumably a network error
  return new Error("unhandleable error");
};
