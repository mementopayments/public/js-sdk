import axios, { AxiosInstance } from "axios";
import { Storage, InMemoryStorage } from "./storage";
import { headers } from "./utils/helpers";
import SessionsEndpoint from "./endpoints/sessions";
import UsersEndpoint from "./endpoints/users";
import FundingSourceEndpoint from "./endpoints/funding_sources";
import DevicesEndpoint from "./endpoints/devices";
import TokensEndpoint from "./endpoints/tokens";
import VerificationEndpoint from "./endpoints/verifications";
import ImageEndpoint from "./endpoints/images";
import NotificationsEndpoint from "./endpoints/notifications";
import AnnouncementsEndpoint from "./endpoints/announcements";
import ReferralsEndpoint from "./endpoints/referrals";
import OnboardingEndpoint from "./endpoints/onboarding";
import ProjectsEndpoint from "./endpoints/projects";
import TransferEndpoint from "./endpoints/transfer";
import PublicTokenEndpoints from "./endpoints/public_tokens";
import PublicTokenEndpoint from "./endpoints/public_tokens";
import { BaseEndpointConfig } from "./endpoints/base";
import TransactionsEndpoint from "./endpoints/transactions";
import CategoriesEndpoint from "./endpoints/categories";
import InvitesEndpoint from "./endpoints/invites";
import WalletEndpoint from "./endpoints/wallet";
import { TeamsEndpoint } from "./endpoints/teams";

type SupportedLocales = "en-US" | "is-IS";

export interface MPPConfig {
  // Required options
  host: string;
  version: string;
  appName: string;
  appVersion: string;

  // Optional options
  autoRefreshSession: boolean;
  storage: Storage;
  overwriteLocale?: SupportedLocales;

  /**
   * Sets the initial project id.
   */
  projectId: string | null;
}

export type MPPConfigInput = Partial<MPPConfig> & {
  host: string;
  appName: string;
  appVersion: string;
};

export class MPPSDK {
  private config: MPPConfig = {
    // This are set in the constructor
    host: "",
    version: "v1",
    appName: "DefaultName",
    appVersion: "0.0.1",

    // Optional config options
    autoRefreshSession: true,
    storage: new InMemoryStorage(),
    projectId: null,
  };

  private http: AxiosInstance;

  public readonly sessions: SessionsEndpoint;
  public readonly users: UsersEndpoint;
  public readonly fundingSources: FundingSourceEndpoint;
  public readonly transfers: TransferEndpoint;
  public readonly devices: DevicesEndpoint;
  public readonly tokens: TokensEndpoint;
  public readonly verifications: VerificationEndpoint;
  public readonly images: ImageEndpoint;
  public readonly notifications: NotificationsEndpoint;
  public readonly announcements: AnnouncementsEndpoint;
  public readonly referrals: ReferralsEndpoint;
  public readonly onboarding: OnboardingEndpoint;
  public readonly projects: ProjectsEndpoint;
  public readonly publicTokens: PublicTokenEndpoint;
  public readonly transactions: TransactionsEndpoint;
  public readonly categories: CategoriesEndpoint;
  public readonly invites: InvitesEndpoint;
  public readonly wallet: WalletEndpoint;
  public readonly teams: TeamsEndpoint;

  constructor(config: MPPConfigInput) {
    this.config = { ...this.config, ...config };

    if (this.config.host === "") {
      throw new Error("Empty host");
    }

    // Just to make sure that we're always working with null instead of
    // potentially undefined.
    if (!this.config.projectId) {
      this.config.projectId = null;
    }

    // Remove old version information from sdk host input
    const rePathVersion = /\/v1\/*$/;
    if (this.config.host.match(rePathVersion)) {
      console.warn(
        "[mpp-sdk] Version information in the host url is deprecated.",
        "Please use the version parameter on the config instead.",
      );
      this.config.host = this.config.host.replace(rePathVersion, "");
      // this.config.host = this.config.host.substring(
      //   0,
      //   this.config.host.length - 2
      // );
    }

    // Remove spaces from appName and appVersion
    this.config.appName = this.config.appName.replace(" ", "");
    this.config.appVersion = this.config.appVersion.replace(" ", "");

    this.http = axios.create({
      baseURL: this.config.host,
    });

    // Set default base headers
    this.http.interceptors.request.use((config) => {
      config.headers = { ...config.headers, ...headers(config, this.config) };
      return config;
    });

    this.tokens = new TokensEndpoint(this.config, this.http);
    this.sessions = new SessionsEndpoint(this.http, this.config, this.tokens);

    const baseConfig: BaseEndpointConfig = {
      config: this.config,
      http: this.http,
      sessions: this.sessions,
    };
    this.users = new UsersEndpoint(baseConfig);
    this.fundingSources = new FundingSourceEndpoint(baseConfig);
    this.devices = new DevicesEndpoint(baseConfig);
    this.verifications = new VerificationEndpoint(baseConfig);
    this.images = new ImageEndpoint(baseConfig);
    this.notifications = new NotificationsEndpoint(baseConfig);
    this.announcements = new AnnouncementsEndpoint(baseConfig);
    this.referrals = new ReferralsEndpoint(baseConfig);
    this.onboarding = new OnboardingEndpoint(baseConfig);
    this.projects = new ProjectsEndpoint(baseConfig);
    this.transfers = new TransferEndpoint(baseConfig);
    this.transactions = new TransactionsEndpoint(baseConfig);
    this.publicTokens = new PublicTokenEndpoints(baseConfig);
    this.categories = new CategoriesEndpoint(baseConfig);
    this.invites = new InvitesEndpoint(baseConfig);
    this.wallet = new WalletEndpoint(
      this.config,
      this.fundingSources,
      this.users,
    );
    this.teams = new TeamsEndpoint(baseConfig);
  }

  async logout() {
    await this.tokens.delete();
    this.sessions.clear();
    await this.config.storage.clearAuthToken();
  }

  async getCurrentDeviceId() {
    return this.tokens.getCurrentDeviceId();
  }

  setProjectId(id: string | null) {
    this.config.projectId = id;
  }
}

export * from "./errors";
export * from "./types";
