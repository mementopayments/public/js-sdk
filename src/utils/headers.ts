export interface LinkHeader {
  uri: string;
  attrs: {
    rel?: string;
    [key: string]: string | undefined;
  };
}

const parseLinkHeaderAttribute = (property: string) => {
  const r = /([a-z]+)\="?([a-zA-Z]+)"?/;
  const match = property.match(r);
  if (!match || match.length < 3) {
    return undefined;
  }
  return [match[1], match[2]] as const;
};

export const parseLinkHeader = (linkHeader: string): LinkHeader[] => {
  const tokens = linkHeader.split(",");

  const links: LinkHeader[] = [];
  for (const token of tokens) {
    const items = token.split("; ");
    if (items.length <= 0) {
      continue;
    }

    // The uri is returned surrounded by angle brackets so we remove them
    const uri = items[0].slice(1, -1);

    const link: LinkHeader = {
      uri,
      attrs: {},
    };

    // Loop through the links attribute list and parse it
    for (let i = 1; i < items.length; i++) {
      // Parse the attribute, if nothing is returned it is considered invalid
      // and skipped
      const attr = parseLinkHeaderAttribute(items[i]);
      if (!attr) {
        continue;
      }

      // Collect the attributes
      const [key, value] = attr;
      link.attrs[key] = value;
    }

    links.push(link);
  }

  return links;
};
