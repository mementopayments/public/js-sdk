import {
  decode as base64Decode,
  encode as base64Encode,
} from "base64-arraybuffer";
import { FundingSourceSensitiveDetails, MPPResult } from "../types";

type GetSensitiveDetails = (
  data: any,
) => Promise<MPPResult<FundingSourceSensitiveDetails>>;

// This is not currently used but is kept in case we are able to use the key
// exchange method again
export const keyPair = async (
  getSensitiveDetails: GetSensitiveDetails,
): Promise<MPPResult<FundingSourceSensitiveDetails>> => {
  // Generate key pair
  // RSA_ECB_OAEP_SHA256_MGF1_2048
  const keyPair = await crypto.subtle.generateKey(
    {
      name: "RSA-OAEP",
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      hash: "SHA-256",
    },
    true,
    ["decrypt", "encrypt"],
  );

  // Export the public key and PEM encode it
  const exported = await crypto.subtle.exportKey("spki", keyPair.publicKey);
  const exportedAsBase64 = base64Encode(exported);
  const pemPublicKey = `-----BEGIN PUBLIC KEY-----\n${exportedAsBase64}\n-----END PUBLIC KEY-----`;

  const response = await getSensitiveDetails({
    encryption_key: pemPublicKey,
  });

  // If we got the encrypted card details we decrypt them
  if (response.data.card_details) {
    // Helper function that decrypts and returns the decrypted value as a
    // string
    const decrypt = async (data: string) => {
      const decrypted = await crypto.subtle.decrypt(
        { name: "RSA-OAEP" },
        keyPair.privateKey,
        base64Decode(data),
      );
      // Encode to string
      return String.fromCharCode.apply(null, [...new Uint8Array(decrypted)]);
    };

    // Decrypt card and cvv number at the same time
    const decryptCardNumber = decrypt(response.data.card_details.card_number);
    const decryptCvv = decrypt(response.data.card_details.cvv);
    await Promise.all([decryptCardNumber, decryptCvv]);

    // Overwrite the response with the decrypted values
    response.data.card_details.card_number = await decryptCardNumber;
    response.data.card_details.cvv = await decryptCvv;
  }

  return response;
};
