import { AxiosRequestConfig } from "axios";
import { MPPConfig } from "../";
import { Filter, FilterOp } from "../types/filters";
import { LIB_VERSION } from "../version";

const FILTER_DELIMITER = ":";
const FILTER_VALUE_DELIMITER = "|";
const NON_IDEMPOTENT_HTTP_METHODS = new Set(["post", "patch"]);

export const isIdempotent = (method: string) =>
  !NON_IDEMPOTENT_HTTP_METHODS.has(method);

export const uuidv4 = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

export type URLOptions = { [key: string]: string };

export const headers = (
  axiosConfig: AxiosRequestConfig,
  mppConfig: MPPConfig,
) => {
  var headers: { [key: string]: string } = {
    "Content-Type": "application/json; charset=utf-8",
    "User-Agent": `${mppConfig.appName}/${mppConfig.appVersion} (JSSDK ${LIB_VERSION})`,
  };

  if (mppConfig.projectId) {
    headers["Project-ID"] = mppConfig.projectId;
  }

  if (mppConfig.overwriteLocale !== undefined) {
    headers["Accept-Language"] = mppConfig.overwriteLocale;
  }

  // Only set idempotency header when using non-idempotent methods.
  const method = axiosConfig.method?.toLowerCase() || "";
  if (!isIdempotent(method)) {
    headers["Idempotency-Key"] = uuidv4();
  }

  return headers;
};

export const filtersToParams = <
  TFilterOp extends FilterOp, // Only here for type inference
  TSortKey extends string, // Only here for type inference
  F extends Filter<TFilterOp, TSortKey>,
>(
  options: F,
) => {
  const params: { [key: string]: string | number } = {};

  if (options.page) {
    params["page"] = options.page;
  }

  if (options.limit) {
    params["limit"] = options.limit;
  }

  if (options.sort) {
    const parts = [];

    for (var s of options.sort) {
      if (s.length !== 2) {
        throw new Error("Invalid sort definition");
      }
      parts.push(s[0] + FILTER_DELIMITER + s[1]);
    }

    params["sort"] = parts.join(FILTER_VALUE_DELIMITER);
  }

  if (options.filter) {
    // Go through all the filters and add to query params map
    const parts: string[] = [];
    const filterKeys = Object.keys(options.filter);

    const handleFilterOp = (key: string, opObj: FilterOp[0]) => {
      if (!opObj) {
        return;
      }

      // Should always get a single op
      const [op] = Object.keys(opObj);
      // Serialize to string
      let x: string;
      const values = opObj[op];
      if (Array.isArray(values)) {
        x = values.join(FILTER_DELIMITER);
      } else {
        x = values;
      }

      // Compile the parts
      parts.push(`${key}${FILTER_DELIMITER}${op}${FILTER_DELIMITER}${x}`);
    };

    const handleFilter = (filter: FilterOp) => {
      for (const key of filterKeys) {
        handleFilterOp(key, filter[key]);
      }
    };

    if (Array.isArray(options.filter)) {
      options.filter.forEach(handleFilter);
    } else {
      handleFilter(options.filter);
    }

    params["filter"] = parts.join(FILTER_VALUE_DELIMITER);
  }

  return params;
};
