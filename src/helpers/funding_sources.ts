import axios from "axios";
import { FundingSourceSensitiveDetailsIFrame } from "../types";

export const getSensitiveDetailsIframeContent = async (
  iframe: FundingSourceSensitiveDetailsIFrame,
) => {
  const iframeData = await axios.request<string>({
    url: iframe.url,
    method: "GET",
    headers: {
      Accept: "*/*",
      Referer: iframe.referer,
    },
  });
  return {
    headers: iframeData.headers,
    data: iframeData.data,
    status: {
      code: iframeData.status,
      text: iframeData.statusText,
    },
  };
};
