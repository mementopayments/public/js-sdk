import { Referral, ReferralInput } from "../types";
import { BaseEndpoint } from "./base";

export default class ReferralsEndpoint extends BaseEndpoint {
  create(data: ReferralInput) {
    return this.doRequest<Referral>({
      url: "/referrals",
      method: "POST",
      data,
    });
  }

  redeem(code: string) {
    return this.doRequest<Referral>({
      url: `/referrals/redeem/${code}`,
      method: "POST",
    });
  }
}
