import { BaseEndpoint } from "./base.js";
import {
  InternalTransaction,
  InternalTransactionFilter,
  ReceiptUploadMethods,
  TransactionInput,
  TransactionReceipt,
} from "../types/transactions.js";
import { filtersToParams } from "../utils/helpers.js";
import { Category, CategoryFilter } from "../types/categories.js";
import { ImageUploadMethods } from "../types/image.js";

export default class TransactionsEndpoint extends BaseEndpoint {
  /**
   * Gets available transactions for current user
   * @returns a list of available transactions
   */
  list(filters?: InternalTransactionFilter) {
    return this.doRequest<InternalTransaction[]>({
      url: "/transactions/",
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }

  /**
   * Gets available transactions for current user with the "missing_data" status
   * @returns a list of available transactions
   */
  todo(filters?: InternalTransactionFilter) {
    return this.doRequest<InternalTransaction[]>({
      url: "/transactions/todo",
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }

  /**
   * Gets a transaction
   * @param data
   * @returns transaction
   */
  get(id: string) {
    return this.doRequest<InternalTransaction>({
      url: `/transactions/${id}`,
      method: "GET",
    });
  }

  /**
   * Gets available transaction categories for current user
   * @returns a list of available transaction categories
   */
  categories(filters?: CategoryFilter) {
    return this.doRequest<Category[]>({
      url: "/transactions/categories",
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }

  /**
   * Updates a transaction
   * @returns the updated transaction object
   */
  update(id: string, data: TransactionInput) {
    return this.doRequest<InternalTransaction>({
      url: `/transactions/${id}`,
      method: "PATCH",
      data,
    });
  }

  /**
   * Gets all receipts fro a transaction
   * @returns a list of receipts for a transactions
   */
  listReceipts(transaction_id: string) {
    return this.doRequest<TransactionReceipt[]>({
      url: "/transactions/${transaction_id}/receipts/",
      method: "GET",
    });
  }

  /**
   * Uploads a receipt for a transaction
   * @returns a transaction receipt
   */
  async uploadReceipt(transaction_id: string, input: ReceiptUploadMethods) {
    const data = new FormData();
    data.append("file", input.file);
    let headers = {
      "Content-Type": "multipart/form-data",
    };
    // form-data nodejs polyfill needs this since axios
    // does not correctly set the multipart form boundary when running
    // in nodejs.
    if ((data as any).getHeaders) {
      headers = (data as any).getHeaders();
    }

    return this.doRequest<TransactionReceipt>({
      url: `/transactions/${transaction_id}/receipts/`,
      method: "POST",
      headers,
      data,
    });
  }

  /**
   * Overrides the team of a transaction.
   * @param transaction_id
   * @param team_id
   * @returns the affected transaction
   */
  setTeam(transaction_id: string, team_id: string) {
    return this.doRequest<InternalTransaction>({
      url: `/transactions/${transaction_id}/team/${team_id}`,
      method: "PUT",
    });
  }
}
