import { BaseEndpoint } from "./base.js";
import { filtersToParams } from "../utils/helpers.js";
import {
  Balance,
  FundingSource,
  FundingSourceFilter,
  FundingSourceInput,
  FundingSourceOperationInput,
  FundingSourceRequestPinResponse,
  FundingSourceSensitiveDetails,
  FundingSourceProvisionApplePayInput,
  FundingSourceProvisionApplePay,
  FundingSourceProvisionGooglePayInput,
  FundingSourceProvisionGooglePay,
  FundingSourceInAppActivation,
  Transaction,
  TransactionFilter,
  TransactionInput,
} from "../types";
import {
  decode as base64Decode,
  encode as base64Encode,
} from "base64-arraybuffer";

export default class FundingSourceEndpoint extends BaseEndpoint {
  list(filters?: FundingSourceFilter) {
    return this.doRequest<FundingSource[]>({
      url: "/funding_sources",
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }

  getById(id: string) {
    return this.doRequest<FundingSource>({
      url: `/funding_sources/${id}`,
      method: "GET",
    });
  }

  /**
   * Creates a new funding source
   * @param data
   * @returns the newly created funding source
   */
  create(data: FundingSourceInput) {
    return this.doRequest<FundingSource>({
      url: `/funding_sources`,
      method: "POST",
      data,
    });
  }

  /**
   * Deletes the specified funding source
   * @param id funding source id
   * @returns empty response
   */
  delete(id: string) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}`,
      method: "DELETE",
    });
  }

  balance(id: string) {
    return this.doRequest<Balance>({
      url: `/funding_sources/${id}/balance`,
      method: "GET",
    });
  }

  verify(id: string, data: { data: string }) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}/verify`,
      method: "POST",
      data,
    });
  }

  lock(id: string, data: FundingSourceOperationInput) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}/lock`,
      method: "POST",
      data,
    });
  }

  unlock(id: string, data: FundingSourceOperationInput) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}/unlock`,
      method: "POST",
      data,
    });
  }

  setPin(id: string, data: { pin: string; new_pin: string }) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}/pin/set`,
      method: "POST",
      data,
    });
  }

  requestPin(id: string, data: { pin: string }) {
    return this.doRequest<FundingSourceRequestPinResponse>({
      url: `/funding_sources/${id}/pin/request`,
      method: "POST",
      data,
    });
  }

  activate(id: string, data: FundingSourceOperationInput) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}/activate`,
      method: "POST",
      data,
    });
  }

  freeze(id: string, data: FundingSourceOperationInput) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}/freeze`,
      method: "POST",
      data,
    });
  }

  unfreeze(id: string, data: FundingSourceOperationInput) {
    return this.doRequest<never>({
      url: `/funding_sources/${id}/unfreeze`,
      method: "POST",
      data,
    });
  }

  provisionApplePay(id: string, data: FundingSourceProvisionApplePayInput) {
    return this.doRequest<FundingSourceProvisionApplePay>({
      url: `/funding_sources/${id}/provision/applepay`,
      method: "POST",
      data,
    });
  }

  provisionGooglePay(id: string, data: FundingSourceProvisionGooglePayInput) {
    return this.doRequest<FundingSourceProvisionGooglePay>({
      url: `/funding_sources/${id}/provision/googlepay`,
      method: "POST",
      data,
    });
  }

  inAppActivation(id: string) {
    return this.doRequest<FundingSourceInAppActivation>({
      url: `/funding_sources/${id}/in_app_activation`,
      method: "POST",
    });
  }

  transactions(id: string, filters?: TransactionFilter) {
    return this.doRequest<Transaction[]>({
      url: `/funding_sources/${id}/transactions`,
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }

  authorizations(id: string, filters?: TransactionFilter) {
    return this.doRequest<Transaction[]>({
      url: `/funding_sources/${id}/authorizations`,
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }

  async sensitiveDetails(id: string) {
    return await this.doRequest<FundingSourceSensitiveDetails>({
      url: `/funding_sources/${id}/sensitive`,
      method: "POST",
      data: {},
    });
  }

  async patchTransaction(
    fsId: string,
    transactionId: string,
    data: TransactionInput,
  ) {
    return this.doRequest<Transaction>({
      url: `/funding_sources/${fsId}/transactions/${transactionId}`,
      method: "PATCH",
      data,
    });
  }
}
