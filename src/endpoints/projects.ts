import { Project } from "../types";
import { BaseEndpoint } from "./base";

export default class ProjectsEndpoint extends BaseEndpoint {
  current() {
    return this.doRequest<Project>({
      url: "/projects/current",
      method: "GET",
    });
  }

  list() {
    return this.doRequest<Project[]>({
      url: "/projects",
      method: "GET",
    });
  }
}
