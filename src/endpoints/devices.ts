import { BaseEndpoint } from "./base";
import {
  Device,
  DeviceInput,
  DeviceKey,
  DeviceKeyInput,
  DeviceTokenInput,
} from "../types";

export default class DevicesEndpoint extends BaseEndpoint {
  getById(id: string) {
    return this.doRequest<Device>({
      url: `/devices/${id}`,
      method: "GET",
    });
  }

  update(deviceId: string, data: DeviceInput) {
    return this.doRequest<Device>({
      url: `/devices/${deviceId}`,
      method: "PUT",
      data,
    });
  }

  /**
   * createToken creates and attaches a device token to the device
   * associated with the authentication token used which is used for
   * e.g. push notifications.
   * @param data
   */
  createToken(data: DeviceTokenInput) {
    return this.doRequest<Device>({
      url: `/devices/tokens`,
      method: "POST",
      data,
    });
  }

  /**
   * Sets the device key for the device associated with the authentication
   * token used.
   * @param data
   */
  setDeviceKey(data: DeviceKeyInput) {
    return this.doRequest<never>({
      url: "/devices/key",
      method: "PUT",
      data,
    });
  }

  /**
   * Gets the device key for the device associated with the authentication
   * token used.
   */
  getDeviceKey() {
    return this.doRequest<DeviceKey>({
      url: "/devices/key",
      method: "GET",
    });
  }
}
