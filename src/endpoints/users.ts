import { BaseEndpoint } from "./base.js";
import { UserSearchInput, PublicUser, UserInput, User } from "../types/user.js";
import { AuthToken, MPPResult } from "../types/index.js";
import { ValidationError } from "../errors.js";

export default class UsersEndpoint extends BaseEndpoint {
  /**
   * Gets the user by the id. Returns regular User object if
   * the user fetched is the current user. Otherwise returns
   * the public user object.
   * @param id user id
   */
  getById(id: string) {
    return this.doRequest<User | PublicUser>({
      url: `/users/${id}`,
      method: "GET",
    });
  }

  search(data: UserSearchInput) {
    return this.doRequest<PublicUser[]>({
      url: "/users/search",
      method: "POST",
      data,
    });
  }

  recents() {
    return this.doRequest<PublicUser[]>({
      url: "/users/recents",
      method: "GET",
    });
  }

  /**
   * Create a user. Newly created users are auto logged in.
   * @param data
   */
  async create(data: UserInput) {
    const result = await this.doRequest<
      User & { authentication_token?: AuthToken }
    >({
      url: "/users",
      method: "POST",
      data,
    });

    // Store auth token and remove from output
    await this.config.storage.storeAuthToken(result.data.authentication_token!);
    delete result.data.authentication_token;

    return result as MPPResult<User>;
  }

  current() {
    return this.doRequest<User>({
      url: "/users/current",
      method: "GET",
    });
  }

  updateCurrent(data: UserInput) {
    return this.doRequest<User>({
      url: "/users/current",
      method: "PATCH",
      data,
    });
  }

  /**
   * Checks whether the given username is taken. Returns true if the username
   * is available.
   * @param username
   */
  async checkUsername(username: string): Promise<boolean> {
    try {
      await this.doRequest<never>({
        url: "/check_username",
        method: "POST",
        data: { username },
      });
      return true;
    } catch (e) {
      if (e instanceof ValidationError) {
        return false;
      }
      throw e;
    }
  }
}
