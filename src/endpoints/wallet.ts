import { GeneralError, MPPConfig } from "..";
import FundingSourceEndpoint from "./funding_sources";
import UsersEndpoint from "./users";
import {
  AppleWalletExtensionAuthToken,
  AppleWalletExtensionCard,
  AppleWalletExtensionData,
} from "../types";

export default class WalletEndpoint {
  constructor(
    protected config: MPPConfig,
    protected fundingSources: FundingSourceEndpoint,
    protected users: UsersEndpoint,
  ) {}

  async getAppleExtensionData(): Promise<AppleWalletExtensionData> {
    try {
      // Get auth token
      const authToken = await this.config.storage.retrieveAuthToken();
      if (!authToken) {
        throw new GeneralError("No auth token");
      }

      // Get current user
      const userResponse = await this.users.current();
      if (!userResponse.data) {
        throw new GeneralError("Unable to retrieve user");
      }

      // Get funding sources
      const fsResponse = await this.fundingSources.list();
      if (!fsResponse.data) {
        throw new GeneralError("Unable to retrieve funding sources");
      }

      // Get available cards
      let fundingSources = fsResponse.data
        .slice()
        .filter((element) => element.status !== "closed");
      let cards: Array<AppleWalletExtensionCard> = [];

      for (let i = 0; i < fundingSources.length; i++) {
        let fundingSource = fundingSources[i];
        if (
          fundingSource &&
          fundingSource.card &&
          fundingSource.card.type_id !== 3
        ) {
          let last4 = fundingSource.card.masked_number.slice(-4);
          cards.push({
            identifier: fundingSource.id,
            title: fundingSource.description || "Kardio",
            last4: last4.trim(),
            cardHolderName: userResponse.data.full_name,
            art: "default",
          });
        }
      }

      // Return the data
      let data: AppleWalletExtensionData = {
        projectId: this.config.projectId || "",
        email: userResponse.data.email || "",
        authToken: {
          token: authToken.token,
          expiresAt: authToken.expires_at,
        },
        cards: cards,
      };

      return data;
    } catch (error) {
      throw new GeneralError(String(error));
    }
  }
}
