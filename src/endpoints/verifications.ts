import { BaseEndpoint } from "./base";
import { VerificationDefintions, VerificationType } from "../types";

export default class VerificationEndpoint extends BaseEndpoint {
  getById<T extends VerificationDefintions>(id: string) {
    return this.doRequest<T["response"]>({
      url: `/verifications/${id}`,
      method: "GET",
    });
  }

  /**
   * Create a verification.
   * Verification object is created but the verification flow is not started.
   * @param data
   */
  create<
    TDef extends VerificationDefintions,
    TType extends VerificationType = TDef["type"],
  >(data: { type: TType } & TDef["initial_data"]) {
    return this.doRequest<{ type: TType } & TDef["response"]>({
      url: "/verifications",
      method: "POST",
      data,
    });
  }

  /**
   * Start a verification.
   * Starts the verification flow for an existing verification.
   * @param verificationId id of verification
   * @param data
   */
  start<T extends VerificationDefintions>(
    verificationId: string,
    data?: T["initial_data"]["data"],
  ) {
    return this.doRequest<T["response"]>({
      url: `/verifications/${verificationId}/start`,
      method: "POST",
      data,
    });
  }

  /**
   * Provide data to an existing verification.
   * @param verificationId id of verification
   * @param data verification data
   */
  provide<T extends VerificationDefintions>(
    verificationId: string,
    data: T["provide_data"],
  ) {
    return this.doRequest<T["response"]>({
      url: `/verifications/${verificationId}/data`,
      method: "POST",
      data,
    });
  }
}
