import { AxiosInstance, AxiosRequestConfig } from "axios";
import SessionsEndpoint from "./sessions";
import { MPPConfig } from "..";
import { errorHandler, MPPError } from "../errors";
import { MPPResult } from "../types/result";

export interface BaseEndpointConfig {
  config: MPPConfig;
  http: AxiosInstance;
  sessions: SessionsEndpoint;
}

export abstract class BaseEndpoint {
  protected config: MPPConfig;
  protected http: AxiosInstance;
  protected sessions: SessionsEndpoint;

  constructor(config: BaseEndpointConfig) {
    this.config = config.config;
    this.http = config.http;
    this.sessions = config.sessions;
  }

  /**
   * Performs an http request using the provided axios config.
   * Auto refreshes session token if that setting is enabled and
   * wraps the response in a MPPResponse.
   * @param config axios request config
   */
  protected async doRequest<T>(
    config: AxiosRequestConfig,
  ): Promise<MPPResult<T>> {
    // Attempt to add session token if we have a valid auth token, otherwise
    // we don't add the session token and just let the api return an error.
    if (await this.sessions.hasAuthToken()) {
      // Check if we should automatically renew session
      if (this.config.autoRefreshSession) {
        if (!this.sessions.hasSession) {
          try {
            await this.sessions.renew();
          } catch (e) {
            // Error is already handled so we rethrow
            if (e instanceof MPPError) {
              throw e;
            }
            throw errorHandler(e);
          }
        }
      }

      // Add auth header to config
      if (!config.headers) {
        config.headers = {};
      }
      config.headers["Authorization"] = `Bearer ${this.sessions.token}`;
    }

    try {
      if (!config.url?.startsWith(`/${this.config.version}`)) {
        config.url = `/${this.config.version}${config.url}`;
      }

      const result = await this.http.request<T>(config);
      return new MPPResult<T>(
        result,
        // Pass in the request handler and bind it to the parent class
        this.doRequest.bind(this),
      );
    } catch (e) {
      throw errorHandler(e);
    }
  }
}
