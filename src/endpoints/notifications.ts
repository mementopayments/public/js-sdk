import { BaseEndpoint } from "./base";
import { filtersToParams } from "../utils/helpers";
import { Notification, Filter } from "../types";

export default class NotificationsEndpoint extends BaseEndpoint {
  list(filters?: Filter) {
    return this.doRequest<Notification[]>({
      url: "/notifications",
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }
}
