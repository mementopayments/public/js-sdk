import { BaseEndpoint } from "./base.js";
import {
  GetPublicTokenResponse,
  PublicTokenInput,
} from "../types/public_token.js";

export default class PublicTokenEndpoint extends BaseEndpoint {
  /**
   * Creates a new temporary public token
   * @param data
   * @returns the newly created temp public token
   */
  create(data: PublicTokenInput) {
    return this.doRequest<GetPublicTokenResponse>({
      url: `/public_tokens`,
      method: "POST",
      data,
    });
  }
}
