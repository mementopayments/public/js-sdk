import { Announcement } from "../types";
import { BaseEndpoint } from "./base";

export default class AnnouncementsEndpoint extends BaseEndpoint {
  list() {
    return this.doRequest<Announcement[]>({
      url: "/announcements",
      method: "GET",
    });
  }

  ack(id: string) {
    return this.doRequest<never>({
      url: `/announcements/${id}/ack`,
      method: "POST",
    });
  }
}
