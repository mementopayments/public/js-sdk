import { BaseEndpoint } from "./base";
import { ImageOptions, Image, ImageUploadMethods } from "../types";
import FormData from "form-data";

type TransactionImageUpload = ImageUploadMethods & {
  type: "transaction";
  funding_source_id: string;
  transaction_id: string;
};

type ProfileImageUpload = ImageUploadMethods & {
  type: "profile";
};

export default class ImageEndpoint extends BaseEndpoint {
  private async internalUpload(
    input: TransactionImageUpload | ProfileImageUpload,
  ) {
    const data = new FormData();

    if ("file" in input) {
      data.append("file", input.file);
    } else if ("url" in input) {
      data.append("url", input.url);
    } else if ("encoded" in input) {
      data.append("encoded", input.encoded);
    }

    if (input.type === "profile") {
      data.append("user_image", "1");
    } else if (input.type === "transaction") {
      data.append("transaction_image", "1");
      data.append("funding_source_id", input.funding_source_id);
      data.append("transaction_id", input.transaction_id);
    }

    let headers = {
      "Content-Type": "multipart/form-data",
    };
    // form-data nodejs polyfill needs this since axios
    // does not correctly set the multipart form boundary when running
    // in nodejs.
    if ((data as any).getHeaders) {
      headers = (data as any).getHeaders();
    }

    return this.doRequest<Image>({
      url: "/images",
      method: "POST",
      headers,
      data,
    });
  }

  async upload(options: ImageOptions) {
    let method: ImageUploadMethods | null = null;
    if (options.file) {
      method = { file: options.file };
    } else if (options.url) {
      method = { url: options.url };
    } else if (options.encoded) {
      method = { encoded: options.encoded };
    }

    if (options.user_image && method !== null) {
      return this.internalUpload({
        ...method,
        type: "profile",
      });
    }
  }

  async uploadProfileImage(method: ImageUploadMethods) {
    return this.internalUpload({
      ...method,
      type: "profile",
    });
  }

  async uploadTransactionImage(
    funding_source_id: string,
    transaction_id: string,
    method: ImageUploadMethods,
  ) {
    return this.internalUpload({
      ...method,
      funding_source_id,
      transaction_id,
      type: "transaction",
    });
  }
}
