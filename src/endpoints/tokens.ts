import {
  AuthenticationError,
  errorHandler,
  LockedUserError,
} from "../errors.js";
import axios, { AxiosInstance, AxiosError } from "axios";
import { MPPConfig } from "../index.js";
import {
  AuthToken,
  AuthTokenInput,
  SecretInput,
  MPPResult,
} from "../types/index.js";

export default class TokensEndpoint {
  constructor(
    protected config: MPPConfig,
    protected http: AxiosInstance,
  ) {}

  async getCurrentDeviceId(): Promise<string | null> {
    const token = await this.config.storage.retrieveAuthToken();
    return token?.device_id || null;
  }

  async hasAuthToken(): Promise<boolean> {
    const token = await this.config.storage.retrieveAuthToken();
    return (
      token != null &&
      token.status === "approved" &&
      new Date(token.expires_at) > new Date()
    );
  }

  async create(data: AuthTokenInput): Promise<MPPResult<AuthToken>> {
    try {
      const response = await this.http.request<AuthToken>({
        url: `/${this.config.version}/tokens`,
        method: "POST",
        data,
      });
      await this.config.storage.storeAuthToken(response.data);
      return new MPPResult<AuthToken>(response);
    } catch (err) {
      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status === 403
      ) {
        throw new LockedUserError(err.message, err.response.data);
      }
      throw errorHandler(err as AxiosError);
    }
  }

  async secret(id: string, data: SecretInput): Promise<MPPResult<AuthToken>> {
    try {
      const response = await this.http.request({
        url: `/${this.config.version}/tokens/${id}/secret`,
        method: "POST",
        data,
      });
      await this.config.storage.storeAuthToken(response.data);
      return new MPPResult<AuthToken>(response);
    } catch (err) {
      throw errorHandler(err as AxiosError);
    }
  }

  async delete(): Promise<void> {
    const authToken = await this.config.storage.retrieveAuthToken();
    if (!authToken) {
      // No authentication token to delete so this is a noop
      return;
    }

    try {
      await this.http.delete(`/${this.config.version}/tokens`, {
        auth: {
          username: authToken.token,
          password: "",
        },
      });
    } catch (e) {
      throw errorHandler(e);
    }
  }
}
