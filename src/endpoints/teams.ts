import { Team, TeamFilter } from "../types";
import { filtersToParams } from "../utils/helpers";
import { BaseEndpoint } from "./base";

export class TeamsEndpoint extends BaseEndpoint {
  list(filters?: TeamFilter) {
    return this.doRequest<Team[]>({
      url: "/teams/",
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }
}
