import {
  OnboardingAddAttributesInput,
  OnboardingCreateInput,
  OnboardingResult,
} from "../types";
import { BaseEndpoint } from "./base";

export default class OnboardingEndpoint extends BaseEndpoint {
  async create(data: OnboardingCreateInput) {
    return await this.doRequest<OnboardingResult>({
      url: "/onboarding",
      method: "POST",
      data,
    });
  }

  async getById(id: string) {
    return await this.doRequest<OnboardingResult>({
      url: `/onboarding/${id}`,
      method: "GET",
    });
  }

  async addAttributes(id: string, data: OnboardingAddAttributesInput) {
    return await this.doRequest<OnboardingResult>({
      url: `/onboarding/${id}/attributes`,
      method: "POST",
      data,
    });
  }

  async addVerification(onboardingId: string, verificationId: string) {
    return await this.doRequest<OnboardingResult>({
      url: `/onboarding/${onboardingId}/verifications`,
      method: "POST",
      data: {
        verification_id: verificationId,
      },
    });
  }

  async complete(id: string) {
    const result = await this.doRequest<OnboardingResult>({
      url: `/onboarding/${id}/complete`,
      method: "POST",
    });

    if (
      result.data.status === "approved" ||
      result.data.status === "user_created"
    ) {
      await this.config.storage.storeAuthToken(
        result.data.authentication_token,
      );
    }

    return result;
  }
}
