import { AuthenticationError, errorHandler, LockedUserError } from "../errors";
import { MPPConfig } from "../index.js";
import { SessionToken } from "../types/index.js";
import axios, { AxiosInstance } from "axios";
import TokensEndpoint from "./tokens";

export default class SessionsEndpoint {
  private renewingSessionPromise: Promise<void> | null = null;
  private session: SessionToken | null = null;

  constructor(
    protected http: AxiosInstance,
    protected config: MPPConfig,
    protected tokens: TokensEndpoint,
  ) {}

  get hasSession(): boolean {
    return this.session != null && this.session.expires_at > new Date();
  }

  get token(): string | undefined {
    return this.session?.token;
  }

  hasAuthToken() {
    return this.tokens.hasAuthToken();
  }

  async renew() {
    if (this.renewingSessionPromise) {
      return this.renewingSessionPromise;
    }

    this.renewingSessionPromise = (async () => {
      try {
        const hasAuthToken = await this.tokens.hasAuthToken();
        if (!hasAuthToken) {
          throw new AuthenticationError("No Authentication Token Provided");
        }

        const { data } = await this.create();
        data.expires_at = new Date(data.expires_at);
        data.created_at = new Date(data.created_at);
        data.updated_at = new Date(data.updated_at);
        this.session = data;
      } catch (err) {
        throw err;
      } finally {
        this.renewingSessionPromise = null;
      }
    })();

    return this.renewingSessionPromise;
  }

  async create() {
    const authToken = await this.config.storage.retrieveAuthToken();
    if (!authToken) {
      throw new AuthenticationError("No Authentication Token Provided");
    }

    try {
      return await this.http.post<SessionToken>(
        `/${this.config.version}/sessions`,
        null,
        {
          auth: {
            username: authToken.token,
            password: "",
          },
        },
      );
    } catch (err) {
      if (
        axios.isAxiosError(err) &&
        err.response &&
        err.response.status === 403
      ) {
        throw new LockedUserError(err.message, err.response.data);
      }
      throw errorHandler(err);
    }
  }

  async verify() {
    try {
      return this.http.get(`/${this.config.version}/sessions/verify`);
    } catch (err) {
      throw errorHandler(err);
    }
  }

  clear() {
    this.session = null;
  }
}
