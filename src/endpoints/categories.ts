import { BaseEndpoint } from "./base.js";
import {
  Category,
  CategoryFilter,
  CategoryGroup,
} from "../types/categories.js";
import { filtersToParams } from "../utils/helpers.js";

export default class CategoriesEndpoint extends BaseEndpoint {
  /**
   * Gets available categories for current user
   * @returns a list of available categories
   */
  list(filters?: CategoryFilter) {
    return this.doRequest<Category[]>({
      url: "/categories",
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }

  /**
   * Gets available dimensions
   * @returns a list of available dimensions
   */
  listDimensions() {
    return this.doRequest<CategoryGroup[]>({
      url: "/category_groups",
      method: "GET",
    });
  }

  /**
   * Gets available values for a given dimension
   * @returns a list of available values for a given dimension
   */
  listValues(group_id: string, filters?: CategoryFilter) {
    return this.doRequest<Category[]>({
      url: `/category_groups/${group_id}/categories`,
      method: "GET",
      params: filtersToParams(filters || {}),
    });
  }
}
