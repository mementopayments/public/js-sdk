import { BaseEndpoint } from "./base.js";
import { ProjectInvite } from "../types";

export default class InvitesEndpoint extends BaseEndpoint {
  getById(id: string) {
    return this.doRequest<ProjectInvite>({
      url: `/invites/${id}`,
      method: "GET",
    });
  }

  accept(id: string, token: string) {
    return this.doRequest({
      url: `/invites/${id}/accept`,
      method: "POST",
      data: { token },
    });
  }
}
