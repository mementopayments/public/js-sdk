import { BaseEndpoint } from "./base.js";
import { Transfer, TransferInput, TransferType } from "../types/transfer.js";

export default class TransferEndpoint extends BaseEndpoint {
  /**
   * Gets available transfer types for moving funds
   * @returns a list of available transfer types
   */
  list() {
    return this.doRequest<TransferType[]>({
      url: "/transfers/types",
      method: "GET",
    });
  }

  /**
   * Creates a new transfer
   * @param data
   * @returns the newly created transfer
   */
  create(data: TransferInput) {
    return this.doRequest<Transfer>({
      url: `/transfers`,
      method: "POST",
      data,
    });
  }

  /**
   * Initiate new transfer
   * @returns the transfer object with status
   */
  initiate(id: string) {
    return this.doRequest<Transfer>({
      url: `/transfers/${id}/initiate`,
      method: "POST",
    });
  }
}
