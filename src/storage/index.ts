import { AuthToken } from "../types";

export interface Storage {
  storeAuthToken(authToken: AuthToken): Promise<void>;
  clearAuthToken(): Promise<void>;
  retrieveAuthToken(): Promise<AuthToken | null>;
}

export * from "./in_memory_storage";
export * from "./local_storage";
