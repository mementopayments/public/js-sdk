import { Storage } from "./";
import { AuthToken } from "../types";
import { InMemoryStorage } from "./in_memory_storage";

const DEFAULT_LOCAL_STORAGE_KEY = "mpp-auth-token";

/**
 * This class is for testing purposes only. Please read into how to
 * securely store your auth tokens.
 */
export class LocalStorage implements Storage {
  // We cache token storage using InMemoryStorage because
  // local storage can be slow.
  private cache: InMemoryStorage;

  constructor(private localStorageKey = DEFAULT_LOCAL_STORAGE_KEY) {
    this.cache = new InMemoryStorage();
  }

  async storeAuthToken(authToken: AuthToken | null) {
    if (authToken == null) {
      localStorage.removeItem(this.localStorageKey);
    } else {
      localStorage.setItem(this.localStorageKey, JSON.stringify(authToken));
    }

    await this.cache.storeAuthToken(authToken);
  }

  async clearAuthToken() {
    localStorage.removeItem(this.localStorageKey);
    await this.cache.clearAuthToken();
  }

  async retrieveAuthToken(): Promise<AuthToken | null> {
    // Check the cache for the auth token first
    const token = await this.cache.retrieveAuthToken();
    if (token != null) {
      return token;
    }

    // Otherwise we fall back to local storage
    const val = localStorage.getItem(this.localStorageKey);
    if (!val) {
      return null;
    }
    return JSON.parse(val);
  }
}
