import { Storage } from "./";
import { AuthToken } from "../types";

export class InMemoryStorage implements Storage {
  private authToken: AuthToken | null = null;

  constructor(authToken?: AuthToken) {
    this.authToken = authToken || null;
  }

  async storeAuthToken(authToken: AuthToken | null): Promise<void> {
    this.authToken = authToken;
  }

  async clearAuthToken(): Promise<void> {
    this.authToken = null;
  }

  async retrieveAuthToken(): Promise<AuthToken | null> {
    return this.authToken;
  }
}
