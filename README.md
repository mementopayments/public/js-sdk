# Memento Payments SDK

Simplify your communication with the Memento Payments Platform.

## Installation

### Node.js

```
npm i mpp-sdk
```

### Browser

```html
<script src="https://unpkg.com/mpp-sdk/dist/mpp.bundle.min.js"></script>
```

## Usage

### Node.js

```js
import { MPPSDK } from "mpp-sdk";

const config = {
  host: "API_URL",
  projectId: "YOUR_PROJECT_ID_HERE",
};

const mpp = new MPPSDK(config);
```

### Browser

```html
<script>
  const mpp = new MPPSDK.MPPSDK(config);
  // ...
</script>
```
