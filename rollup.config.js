import { terser } from "rollup-plugin-terser";
import resolve, { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";

export default {
  input: "lib/index.js",
  output: [
    {
      file: "dist/mpp.bundle.min.js",
      format: "iife",
      name: "MPPSDK",
      plugins: [terser()],
    },
  ],
  plugins: [
    resolve({ preferBuiltins: false, browser: true }),
    commonjs(),
    json(),
    nodeResolve(),
  ],
};
